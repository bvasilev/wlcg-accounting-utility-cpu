#!/usr/bin/env python3

import os
import argparse
import subprocess
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

import cpu_pledge
import publisher

def get_data_for_period(months, backup_path, user, password):
    this_month = date.today().replace(day=1)
    current_date = this_month - relativedelta(months=months - 1)
    
    while current_date <= this_month:
        print(f'Getting data for {current_date.month}/{current_date.year}...')
        fpath = cpu_pledge.get_data_for_month(current_date.year, current_date.month, backup_path)
        publisher.publish(user, password, fpath)
        current_date += relativedelta(months=1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get CRIC CPU pledge data for a period')
    parser.add_argument('-u', '--user', type=str, metavar='', required=True, help='Username')
    parser.add_argument('-p', '--password', type=str, metavar='', required=True, help='Password')
    parser.add_argument('-m', '--months', type=int, metavar='', default=1, help='Months before current date to be pulled. Default: 1')
    parser.add_argument('-b', '--backup', type=str, metavar='', required=True, help='Path to backup folder')

    args = parser.parse_args()

    backup_path = os.path.join(args.backup, str(datetime.now()))

    if not os.path.isdir(backup_path):
        print('Creating backup directory')
        os.makedirs(backup_path)

    get_data_for_period(args.months, backup_path, args.user, args.password)
