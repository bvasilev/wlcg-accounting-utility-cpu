#!/usr/bin/python3

import argparse
from urllib.request import urlopen
import json
from datetime import datetime, timezone
from calendar import monthrange


def get_data_for_month_per_tier(year, month):
    pledge_url = "http://wlcg-cric.cern.ch/api/core/federation/query/?json&start_year=2009"
    pledge_response = urlopen(pledge_url)

    pledge_data = pledge_response.read().decode("utf-8")
    pledge_data = json.loads(pledge_data)

    cpu_stats = []

    VO_MAPPING = {
        'alice': 'ALICE',
        'atlas': 'ATLAS',
        'cms': 'CMS',
        'lhcb': 'LHCb',
    }
    for federation in pledge_data.keys():
        pledges = {}
        if str(year) in pledge_data[federation]['pledges']:
            if int(month) in [4, 5, 6]:
                quarter = 'Q1'
                pledge_year = str(year)
            elif int(month) in [7, 8, 9]:
                quarter = 'Q2'
                pledge_year = str(year)
            elif int(month) in [10, 11, 12]:
                quarter = 'Q3'
                pledge_year = str(year)
            else:
                quarter = 'Q4'
                pledge_year = str(year - 1)
            pledges = pledge_data[federation]['pledges'][pledge_year][quarter]
        else:
            continue
        for vo, pledge in pledges.items(): 
            if 'CPU' in pledge:
                cpu_stats.append({
                    'pledge': pledge['CPU'] * 24 * monthrange(year, month)[1],
                    'vo': VO_MAPPING[vo],
                    'tier': 'Tier-' + str(pledge_data[federation]['tier_level']),
                    'country': pledge_data[federation]['country_code'],
                    'federation': pledge_data[federation]['name']
                })

    dt = datetime(year, month, 1, tzinfo=timezone.utc)
    epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
    timestamp = int((dt - epoch).total_seconds() * 1000)

    for entry in cpu_stats:
        entry['timestamp'] = timestamp
        entry['idb_tags'] = ['vo', 'tier', 'country',
                             'federation']
        entry['producer'] = 'wlcgops'
        entry['type'] = 'accounting.cpu.pledges'

    desired_order_list = ['vo', 'tier', 'country', 'federation', 'pledge',
                          'idb_tags', 'producer', 'type', 'timestamp']

    for i in range(len(cpu_stats)):
        cpu_stats[i] = {k: cpu_stats[i][k] for k in desired_order_list}

    return cpu_stats


def get_data_for_month(year, month, backup_path):
    cpu_stats = []

    cpu_stats.extend(get_data_for_month_per_tier(year, month))

    fpath = f'{backup_path}/data_cpu_pledge_{year}_{month}.json'
    with open(fpath, 'w+') as f:
        print(json.dumps(cpu_stats, indent=4), file=f)
    return fpath


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get CRIC CPU pledge data for a month')
    parser.add_argument('year', type=int, help='Year')
    parser.add_argument('month', type=int, help='Month')
    parser.add_argument('-b', '--backup', type=str, metavar='', required=True, help='Path to backup folder')

    args = parser.parse_args()

    get_data_for_month(args.year, args.month, args.backup)
