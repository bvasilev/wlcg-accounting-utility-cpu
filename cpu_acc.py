#!/usr/bin/python3

import argparse
from urllib.request import urlopen
import json
from datetime import datetime, timezone


def get_data_for_month_per_tier(tier, year, month):
    if tier == 'TIER1':
        option_tier = 'TIER1'
        tree_tier = 'TIER1'
    else:
        option_tier = 'COUNTRY_T2'
        tree_tier = 'TIER2'
        
    normelap_url = "https://accounting-support.egi.eu/custom_xml.php?query=normelap_processors&option=%s&sYear=%d&sMonth=%d&eYear=%d&eMonth=%d"\
                   "&xrange=VO&yrange=SITE&groupVO=lhc&localJobs=localinfrajobs&tree=%s&optval=" % (option_tier, year, month, year, month, tree_tier)

    elap_url = "https://accounting-support.egi.eu/custom_xml.php?query=elap_processors&option=%s&sYear=%d&sMonth=%d&eYear=%d&eMonth=%d"\
               "&xrange=VO&yrange=SITE&groupVO=lhc&localJobs=localinfrajobs&tree=%s&optval=" % (option_tier, year, month, year, month, tree_tier)

    cpueff_url = "https://accounting-support.egi.eu/custom_xml.php?query=cpueff&option=%s&sYear=%d&sMonth=%d&eYear=%d&eMonth=%d"\
               "&xrange=VO&yrange=SITE&groupVO=lhc&localJobs=localinfrajobs&tree=%s&optval=" % (option_tier, year, month, year, month, tree_tier)     

    normelap_response = urlopen(normelap_url)
    elap_response = urlopen(elap_url)
    cpueff_response = urlopen(cpueff_url)
    
    normelap_data = normelap_response.read().decode("utf-8")
    elap_data = elap_response.read().decode("utf-8")
    cpueff_data = cpueff_response.read().decode("utf-8")
    normelap_data = json.loads(normelap_data)
    elap_data = json.loads(elap_data)
    cpueff_data = json.loads(cpueff_data)
    
    cric_url = "http://wlcg-cric.cern.ch/api/core/rcsite/query/?json&state=ANY"
    cric_response = urlopen(cric_url)

    cric_data = cric_response.read().decode("utf-8")
    cric_data = json.loads(cric_data)

    cpu_stats = {}
    
    for site in normelap_data:

        if site["id"] == 'CERN-PROD':
            tier_name = 'Tier-0'
        elif tier == 'TIER1':
            tier_name = 'Tier-1'
        else:
            tier_name = 'Tier-2'

        if site["id"] not in ("Total", "Percent", "xlegend", "ylegend", "var"):
                 
                 if site["alice"] != 0:
                     cpu_stats[site["id"] + "_alice"] = {
                            "vo": "ALICE",
                            "site": site["id"],
                            "raw_wc_work": site["alice"],
                            "tier": tier_name
                         }
                 if site["atlas"] != 0:
                     cpu_stats[site["id"] + "_atlas"] = {
                            "vo": "ATLAS",
                            "site": site["id"],
                            "raw_wc_work": site["atlas"],
                            "tier": tier_name
                         }
                 if site["cms"] != 0:
                     cpu_stats[site["id"] + "_cms"] = {
                            "vo": "CMS",
                            "site": site["id"],
                            "raw_wc_work": site["cms"],
                            "tier": tier_name
                         }
                 if site["lhcb"] != 0:
                     cpu_stats[site["id"] + "_lhcb"] = {
                            "vo": "LHCb",
                            "site": site["id"],
                            "raw_wc_work": site["lhcb"],
                            "tier": tier_name
                         }
                        
    for site in elap_data:
        if site["id"] not in ("Total", "Percent", "xlegend", "ylegend", "var"):
                 if site["id"] + "_alice" in cpu_stats:
                     cpu_stats[site["id"] + "_alice"]["raw_wc_time"] = site["alice"]
                 if site["id"] + "_atlas" in cpu_stats:
                     cpu_stats[site["id"] + "_atlas"]["raw_wc_time"] = site["atlas"]
                 if site["id"] + "_cms" in cpu_stats:
                     cpu_stats[site["id"] + "_cms"]["raw_wc_time"] = site["cms"]
                 if site["id"] + "_lhcb" in cpu_stats:
                     cpu_stats[site["id"] + "_lhcb"]["raw_wc_time"] = site["lhcb"]

    for site in cpueff_data:
        if site["id"] not in ("Total", "Percent", "xlegend", "ylegend", "var"):
            if site["id"] + "_alice" in cpu_stats:
                cpu_stats[site["id"] + "_alice"]["raw_cpu_eff"] = int(site["alice"]) / 100.0
            if site["id"] + "_atlas" in cpu_stats:
                cpu_stats[site["id"] + "_atlas"]["raw_cpu_eff"] = int(site["atlas"]) / 100.0
            if site["id"] + "_cms" in cpu_stats:
                cpu_stats[site["id"] + "_cms"]["raw_cpu_eff"] = int(site["cms"]) / 100.0
            if site["id"] + "_lhcb" in cpu_stats:
                cpu_stats[site["id"] + "_lhcb"]["raw_cpu_eff"] = int(site["lhcb"]) / 100.0
    

                        
    for site_name in cric_data.keys():
        if site_name + "_alice" in cpu_stats:
            cpu_stats[site_name + "_alice"]["country"] = cric_data[site_name]["country_code"]
            cpu_stats[site_name + "_alice"]["federation"] = cric_data[site_name]["federation"]
        if site_name + "_atlas" in cpu_stats:
            cpu_stats[site_name + "_atlas"]["country"] = cric_data[site_name]["country_code"]
            cpu_stats[site_name + "_atlas"]["federation"] = cric_data[site_name]["federation"]
        if site_name + "_cms" in cpu_stats:
            cpu_stats[site_name + "_cms"]["country"] = cric_data[site_name]["country_code"]
            cpu_stats[site_name + "_cms"]["federation"] = cric_data[site_name]["federation"]
        if site_name + "_lhcb" in cpu_stats:
            cpu_stats[site_name + "_lhcb"]["country"] = cric_data[site_name]["country_code"]
            cpu_stats[site_name + "_lhcb"]["federation"] = cric_data[site_name]["federation"]

    if 'Purdue-Carter' not in cric_data.keys():
        if 'Purdue-Carter' + "_alice" in cpu_stats:
            cpu_stats['Purdue-Carter' + "_alice"]["country"] = 'US'
            cpu_stats['Purdue-Carter' + "_alice"]["federation"] = 'T2_US_Purdue'
        if 'Purdue-Carter' + "_atlas" in cpu_stats:
            cpu_stats['Purdue-Carter' + "_atlas"]["country"] = 'US'
            cpu_stats['Purdue-Carter' + "_atlas"]["federation"] = 'T2_US_Purdue'
        if 'Purdue-Carter' + "_cms" in cpu_stats:
            cpu_stats['Purdue-Carter' + "_cms"]["country"] = 'US'
            cpu_stats['Purdue-Carter' + "_cms"]["federation"] = 'T2_US_Purdue'
        if site_name + "_lhcb" in cpu_stats:
            cpu_stats['Purdue-Carter' + "_lhcb"]["country"] = 'US'
            cpu_stats['Purdue-Carter' + "_lhcb"]["federation"] = 'T2_US_Purdue'
    
    dt = datetime(year, month, 1, tzinfo=timezone.utc)
    epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
    timestamp = int((dt - epoch).total_seconds() * 1000)
    
    for entry in cpu_stats.values():    
        entry['timestamp'] = timestamp
        entry['idb_tags'] = ['vo', 'tier', 'country',
                        'federation', 'site']
        entry['producer'] = 'wlcgops'
        entry['type'] = 'accounting.wau.summary'
    
    desired_order_list = ['vo', 'tier', 'country', 'federation',
                          'site', 'raw_wc_time', 'raw_wc_work', 'raw_cpu_eff',
                          'idb_tags', 'producer', 'type', 'timestamp']
    
    cpu_stats = list(cpu_stats.values())
    
    for i in range(len(cpu_stats)):
        cpu_stats[i] = {k: cpu_stats[i][k] for k in desired_order_list}
        
    return cpu_stats

def get_data_for_month(year, month, backup_path):
    cpu_stats = []
    
    cpu_stats.extend(get_data_for_month_per_tier('TIER1', year, month))
    cpu_stats.extend(get_data_for_month_per_tier('TIER2', year, month))
    
    fpath = f'{backup_path}/data_cpu_acc_{year}_{month}.json'
    with open(fpath, 'w+') as f:
        print(json.dumps(cpu_stats, indent=4), file=f)
    return fpath


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get EGI CPU accounting data for a month')
    parser.add_argument('year', type=int, help='Year')
    parser.add_argument('month', type=int, help='Month')
    parser.add_argument('-b', '--backup', type=str, metavar='', required=True, help='Path to backup folder')

    args = parser.parse_args()

    get_data_for_month(args.year, args.month, args.backup)
