#!/usr/bin/python3

# Copyright 2017-2019 CERN
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file
# LICENCE.md.
#
# In applying this licence, CERN does not waive the privileges and
# immunities granted to it by virtue of its status as an
# Intergovernmental Organisation or submit itself to any jurisdiction.

import os
import copy
import json
import logging
import sys
import uuid
import argparse
from urllib.request import urlopen
from datetime import date, datetime, timezone
from dateutil.relativedelta import relativedelta
from calendar import monthrange

from stompest.config import StompConfig
from stompest.error import StompError
from stompest.protocol import StompFrame, StompSpec
from stompest.sync import Stomp


class Publisher:

    def __init__(self, host, port, username, password, topic):
        """Initialise this object."""
        stomp_config = StompConfig(uri='tcp://{}:{}'.format(host, port),
                                   login=username,
                                   passcode=password)
        self._client = Stomp(stomp_config)
        self._destination = '/topic/{}'.format(topic)

    def __enter__(self):
        """Enter the runtime context related to this object."""
        self._client.connect()
        return self

    def __exit__(self, type, value, traceback):
        """Exit the runtime context related to this object."""
        self._client.disconnect()
        return type is None

    def send(self, documents):
        """Send a list of documents to the message broker."""
        prefix = str(uuid.uuid1())

        with self._client.transaction(receipt=prefix) as transaction:
            self._expect_receipt('{}-begin'.format(prefix))
            for entry in documents:
                message = copy.deepcopy(entry)
                headers = {StompSpec.TRANSACTION_HEADER: transaction}
                self._client.send(self._destination,
                                  json.dumps(message).encode(),
                                  headers)

        self._expect_receipt('{}-commit'.format(prefix))
        _logger.info('Submitted %d documents', len(documents))

    def _expect_receipt(self, receipt_id, timeout=60):
        """Wait for a RECEIPT frame and verify its ID."""
        if not self._client.canRead(timeout):
            raise StompReceiptError('read timeout expired')
        frame = self._client.receiveFrame()
        frame.unraw()
        response = dict(frame)

        if response['command'] != StompSpec.RECEIPT:
            raise StompReceiptError('frame is not of type RECEIPT')
        elif response['headers'][StompSpec.RECEIPT_ID_HEADER] != receipt_id:
            raise StompReceiptError('frame has unexpected ID')


class StompReceiptError(StompError):
    """Raised for failing to receive a proper RECEIPT frame."""


_logger = logging.getLogger('wssa.publisher')

def publish(user, password, file):
    config = {
        'host': 'dashb-mb.cern.ch',
        'port': 61113,
        'username': user,
        'password': password,
        'topic': 'wlcgops.accounting.space',
    }
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(name)s %(levelname)s: %(message)s'
    )
    with Publisher(**config) as pub:
        _logger.info('Reading from file %s', file)
        with open(file) as f:
            pub.send(json.load(f))


def get_data_for_month_per_tier(year, month):
    pledge_url = "http://wlcg-cric.cern.ch/api/core/federation/query/?json&start_year=2009"
    pledge_response = urlopen(pledge_url)

    pledge_data = pledge_response.read().decode("utf-8")
    pledge_data = json.loads(pledge_data)

    cpu_stats = []

    VO_MAPPING = {
        'alice': 'ALICE',
        'atlas': 'ATLAS',
        'cms': 'CMS',
        'lhcb': 'LHCb',
    }
    for federation in pledge_data.keys():
        pledges = {}
        if str(year) in pledge_data[federation]['pledges']:
            if int(month) in [4, 5, 6]:
                quarter = 'Q1'
                pledge_year = str(year)
            elif int(month) in [7, 8, 9]:
                quarter = 'Q2'
                pledge_year = str(year)
            elif int(month) in [10, 11, 12]:
                quarter = 'Q3'
                pledge_year = str(year)
            else:
                quarter = 'Q4'
                pledge_year = str(year - 1)
            pledges = pledge_data[federation]['pledges'][pledge_year][quarter]
        else:
            continue
        for vo, pledge in pledges.items(): 
            if 'CPU' in pledge:
                cpu_stats.append({
                    'pledge': pledge['CPU'] * 24 * monthrange(year, month)[1],
                    'vo': VO_MAPPING[vo],
                    'tier': 'Tier-' + str(pledge_data[federation]['tier_level']),
                    'country': pledge_data[federation]['country_code'],
                    'federation': pledge_data[federation]['name']
                })

    dt = datetime(year, month, 1, tzinfo=timezone.utc)
    epoch = datetime(1970, 1, 1, tzinfo=timezone.utc)
    timestamp = int((dt - epoch).total_seconds() * 1000)

    for entry in cpu_stats:
        entry['timestamp'] = timestamp
        entry['idb_tags'] = ['vo', 'tier', 'country',
                             'federation']
        entry['producer'] = 'wlcgops'
        entry['type'] = 'accounting.cpu.pledges'

    desired_order_list = ['vo', 'tier', 'country', 'federation', 'pledge',
                          'idb_tags', 'producer', 'type', 'timestamp']

    for i in range(len(cpu_stats)):
        cpu_stats[i] = {k: cpu_stats[i][k] for k in desired_order_list}

    return cpu_stats


def get_data_for_month(year, month, backup_path):
    cpu_stats = []

    cpu_stats.extend(get_data_for_month_per_tier(year, month))

    fpath = f'{backup_path}/data_cpu_pledge_{year}_{month}.json'
    with open(fpath, 'w+') as f:
        print(json.dumps(cpu_stats, indent=4), file=f)
    return fpath

def get_data_for_period(months, backup_path, user, password):
    this_month = date.today().replace(day=1)
    current_date = this_month - relativedelta(months=months - 1)
    
    while current_date <= this_month:
        print(f'Getting data for {current_date.month}/{current_date.year}...')
        fpath = get_data_for_month(current_date.year, current_date.month, backup_path)
        publish(user, password, fpath)
        current_date += relativedelta(months=1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get CRIC CPU pledge data for a period')
    parser.add_argument('-u', '--user', type=str, metavar='', required=True, help='Username')
    parser.add_argument('-p', '--password', type=str, metavar='', required=True, help='Password')
    parser.add_argument('-m', '--months', type=int, metavar='', default=1, help='Months before current date to be pulled. Default: 1')
    parser.add_argument('-b', '--backup', type=str, metavar='', required=True, help='Path to backup folder')

    args = parser.parse_args()

    backup_path = os.path.join(args.backup, str(datetime.now()))

    if not os.path.isdir(backup_path):
        print('Creating backup directory')
        os.makedirs(backup_path)

    get_data_for_period(args.months, backup_path, args.user, args.password)
