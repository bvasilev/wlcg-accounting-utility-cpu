#!/usr/bin/python3

# Copyright 2017-2019 CERN
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file
# LICENCE.md.
#
# In applying this licence, CERN does not waive the privileges and
# immunities granted to it by virtue of its status as an
# Intergovernmental Organisation or submit itself to any jurisdiction.

import copy
import json
import logging
import sys
import uuid
import argparse

from stompest.config import StompConfig
from stompest.error import StompError
from stompest.protocol import StompFrame, StompSpec
from stompest.sync import Stomp


class Publisher:

    def __init__(self, host, port, username, password, topic):
        """Initialise this object."""
        stomp_config = StompConfig(uri='tcp://{}:{}'.format(host, port),
                                   login=username,
                                   passcode=password)
        self._client = Stomp(stomp_config)
        self._destination = '/topic/{}'.format(topic)

    def __enter__(self):
        """Enter the runtime context related to this object."""
        self._client.connect()
        return self

    def __exit__(self, type, value, traceback):
        """Exit the runtime context related to this object."""
        self._client.disconnect()
        return type is None

    def send(self, documents):
        """Send a list of documents to the message broker."""
        prefix = str(uuid.uuid1())

        with self._client.transaction(receipt=prefix) as transaction:
            self._expect_receipt('{}-begin'.format(prefix))
            for entry in documents:
                message = copy.deepcopy(entry)
                headers = {StompSpec.TRANSACTION_HEADER: transaction}
                self._client.send(self._destination,
                                  json.dumps(message).encode(),
                                  headers)

        self._expect_receipt('{}-commit'.format(prefix))
        _logger.info('Submitted %d documents', len(documents))

    def _expect_receipt(self, receipt_id, timeout=60):
        """Wait for a RECEIPT frame and verify its ID."""
        if not self._client.canRead(timeout):
            raise StompReceiptError('read timeout expired')
        frame = self._client.receiveFrame()
        frame.unraw()
        response = dict(frame)

        if response['command'] != StompSpec.RECEIPT:
            raise StompReceiptError('frame is not of type RECEIPT')
        elif response['headers'][StompSpec.RECEIPT_ID_HEADER] != receipt_id:
            raise StompReceiptError('frame has unexpected ID')


class StompReceiptError(StompError):
    """Raised for failing to receive a proper RECEIPT frame."""


_logger = logging.getLogger('wssa.publisher')

def publish(user, password, file):
    config = {
        'host': 'dashb-mb.cern.ch',
        'port': 61113,
        'username': user,
        'password': password,
        'topic': 'wlcgops.accounting.space',
    }
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(name)s %(levelname)s: %(message)s'
    )
    with Publisher(**config) as pub:
        _logger.info('Reading from file %s', file)
        with open(file) as f:
            pub.send(json.load(f))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Publish data to WSSA ActiveMQ topic')
    parser.add_argument('-u', '--user', type=str, metavar='', required=True, help='Username')
    parser.add_argument('-p', '--password', type=str, metavar='', required=True, help='Password')
    parser.add_argument('-f', '--file', type=str, metavar='', required=True, help='File path')

    args = parser.parse_args()

    publish(args.user, args.password, args.file)

    
